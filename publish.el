;;; publish.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 Stirling Hostetter
;;
;; Author: Stirling Hostetter <stirlhoss@proton.me>
;; Maintainer: Stirling Hostetter <stirlhoss@proton.me>
;; Created: January 06, 2023
;; Modified: January 06, 2023
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://gitlab.com/stirlhoss/stirlhoss.org
;; Package-Requires: ((emacs "24.4"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:
(require 'ox-publish)

(setq org-publish-project-alist
      '(("pages"
         :base-directory "~/stirlhoss.org/org/"
         :base-extension "org"
         :recursive nil
         :publishing-directory "~/stirlhoss.org/html/"
         :publishing-function org-html-publish-to-html

         ;; other styling
         :headline-level 4
         :section-numbers nil
         :with-toc nil
         :with-author t

         ;; link to stylesheet
         :html-head "<link rel=\"stylesheet\" href=\"./style.css\" type=\"text/css\"/>"

         ;; html5
         :html-doctype "html5"
         :html-html5-fancy t

         ;; disable org defaults
         :html-head-include-scripts nil
         :html-head-include-default-style nil

         ;; create header and footer
         :html-preamble "<nav>
                        <a href=\"/\">&lt; Home</a> <a href=\"/blog\">&lt; Blog</a>
                        </nav>
                        <div id=\"updated\">Updated: %C</div>"

         :html-postamble "<div class='footer'>
                        Copyright 2023 %a. <br>
                        Last Updated %C. <br>
                        Built with %c. </div>"
         :auto-sitemap t
         :sitemap-title "stirlhoss.org"
         :sitemap-filename "sitemap.org")

        ("blog"
         :base-directory "~/stirlhoss.org/org/blog/"
         :base-extension "org"
         :publishing-directory "~/stirlhoss.org/html/blog/"
         :publishing-function org-html-publish-to-html

         ;; other styling
         :headline-level 4
         :section-numbers nil
         :with-toc nil
         :with-author t

         ;; html5
         :html-doctype "html5"
         :html-html5-fancy t

         ;; disable org defaults
         :html-head-include-scripts nil
         :html-head-include-default-style nil

         ;; create header and footer
         :html-preamble "<nav>
                        <a href=\"/\">&lt; Home</a>
                        </nav>
                        <div id=\"updated\">Updated: %C</div>"

         :html-postamble "<div class='footer'>
                        Copyright 2023 %a. <br>
                        Last Updated %C. <br>
                        Built with %c. </div>"

         ;; link to stylesheet
         :html-head "<link rel=\"stylesheet\" href=\"../style.css\" type=\"text/css\"/>"
         ;; sitemapping
         :auto-sitemap t
         :sitemap-title "Blog Posts"
         :sitemap-filename "index.org"
         :sitemap-sort-files anti-chronologically)

        ("static"
         :base-directory "~/stirlhoss.org/org/"
         :base-extension "css\\|txt\\|jpg\\|gif\\|png"
         :recursive t
         :publishing-directory "~/stirlhoss.org/html/"
         :publishing-function org-publish-attachment)

        ("stirlhoss.org" :components ("pages" "blog" "static"))))

(provide 'publish)
;;; publish.el ends here
